package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.br.projetogerenciadordesenha.Banco.UsuarioDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerUsuario;


public class ActivityEsqueciSenhaEnviarCodigo extends AppCompatActivity implements View.OnClickListener {

    private Button btnEnviarEmail, btnVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueci_senha_enviar_codigo);

        btnEnviarEmail = (Button) findViewById(R.id.btnEnviarCodigo);
        btnEnviarEmail.setOnClickListener(this);

        btnVoltar = (Button) findViewById(R.id.btnVoltarLogin);
        btnVoltar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnviarCodigo:
                try {
                    new UsuarioDAO(getApplication());
                    String[] retorno = ControllerUsuario.alterarCodigo(((EditText) findViewById(R.id.txtEnviarCodigo)).getText().toString().trim(), this);

                    if (retorno[0].equals("0")) {
                        enviarCodigoEmail(retorno[3], retorno[2]);

                        Intent it = new Intent(this, ActivityValidaCodigo.class);
                        it.putExtra("ID_USUARIO", retorno[1]);
                        it.putExtra("EMAILUSUARIO", retorno[2]);
                        it.putExtra("CODIGOVALIDADOR", retorno[3]);
                        it.putExtra("LOGINUSUARIO", retorno[4]);
                        startActivity(it);
                    } else
                        Toast.makeText(this, "Não existe nenhum usuário cadastrado com esse email.", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(this, "ERRO EM ALTERAR CÓDIGO DE USUÁRIO.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnVoltarLogin:
                finish();
                break;
        }
    }

    private void enviarCodigoEmail(String codigo, String email) {
        try {
            ControllerUsuario.enviarCodigoEmail(codigo, email, this);
            Toast.makeText(this, "Código enviado.", Toast.LENGTH_LONG).show();
        } catch (Exception e){
            Toast.makeText(this, "ERRO EM ENVIO DE CÓDIGO PARA EMAIL.", Toast.LENGTH_LONG).show();
        }
    }

}