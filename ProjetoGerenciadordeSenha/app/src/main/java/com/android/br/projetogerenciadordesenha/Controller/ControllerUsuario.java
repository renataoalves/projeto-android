package com.android.br.projetogerenciadordesenha.Controller;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.UsuarioDAO;
import com.android.br.projetogerenciadordesenha.Model.Usuario;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by mag.luiza on 03/12/2015.
 */
public class ControllerUsuario {

    public static String[] validacaoLoginSenha(String login, String senha, Context context) {
        try {
            return UsuarioDAO.validacaoLoginSenha(login, encriptarSenha(senha, context));
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM validacaoLoginSenha() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }
    public static String validarLoginExiste(String login, Context context){
        try{
            return UsuarioDAO.validacaoLoginExiste(login);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM validarLoginExiste() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }
    public static String[] validarEmailExiste(String email, Context context){
        try{
            return UsuarioDAO.procurarEmail(email);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM validarEmailExiste() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static void cadastrarUsuario(String login, String senha, String email, Context context){
        try{
            String codigoValidador = validador(context);
            UsuarioDAO.insert(new Usuario(login, encriptarSenha(senha, context), email, codigoValidador));
            enviarCodigoEmail(email, codigoValidador, context);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM cadastrarUsuario() DE TELA.", Toast.LENGTH_LONG).show();
        }
    }

    public static void alterarSenha(String ID_USUARIO, String novaSenha, Context context){
        try{
            UsuarioDAO.alterarSenha(ID_USUARIO, encriptarSenha(novaSenha, context));
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM alterarSenha() DE TELA.", Toast.LENGTH_LONG).show();
        }
    }
    public static String[] alterarCodigo(String email, Context context){
        try{
            String[] retorno = UsuarioDAO.procurarEmail(email);

            if(!retorno[0].equals("-1")) {
                String codigo = gerarCodigo(context);

                Log.e("contas", codigo);

                UsuarioDAO.alterarCodigo(retorno[0], codigo);

                return new String[] {"0", retorno[0], email, codigo, retorno[1]};
            }else
                return new String[] {"-1"};
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM alterarCodigo() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static void enviarCodigoEmail(String email, String codigo, Context context) {
        try{
            new SendMail().execute(new String[]{email, codigo});
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM enviarCodigoEmail() DE TELA.", Toast.LENGTH_LONG).show();
        }
    }
    private static class SendMail extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... params) {
            Mail m = new Mail();

            m.set_user("gerenciadordesenhaandroid@gmail.com");
            m.set_pass("paulorenato");

            Log.e("contas", "EMAIl: " + params[1]);
            Log.e("contas", "CODIGO: " + params[0]);

            if(params[0].contains("@"))
                m.set_to(new String[]{params[0], params[0]});
            else
                m.set_to(new String[]{params[1], params[1]});

            m.set_from("secrelty.com@gmail.com");
            m.set_subject("RECUPERAÇÃO DE SENHA - GERENCIADOR DE SENHA NATOW");
            m.setBody("Digite o seguinte código em seu dispositivo: "+ params[0] +"\n\nOBS: DIGITE EXATAMENTE COMO SE ENCONTRA.");

            try {
                if(m.send())
                    Log.e("contas", "Email FOI enviado com sucesso.");
                else
                    Log.e("contas", "Email NÃO foi enviado com sucesso.");
            } catch(Exception e) {
                Log.e("contas", "Could not send email", e);
            }
            return null;
        }
        protected void onProgressUpdate() {}
        protected void onPreExecute() {}
        protected void onPostExecute() {}
    }

    private static String gerarCodigo(Context context){
        try{
            return validador(context);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM gerarCodigo() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }
    public static String validador(Context context){
        try{
            String[] caracteres ={"0","1","2","3","4","5","6","7","8","9",
                    "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                    "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

            String validador = "";

            for (int i = 0; i < 5; i++)
                validador += caracteres[new Random().nextInt(caracteres.length)];

            return validador;
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM validador() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    private static String encriptarSenha(String senha, Context context) {
        try{
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return new BigInteger(1, md.digest(senha.getBytes())).toString(16);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM validador() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }

}