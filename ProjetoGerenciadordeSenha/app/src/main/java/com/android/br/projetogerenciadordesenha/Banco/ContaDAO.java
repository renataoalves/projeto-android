package com.android.br.projetogerenciadordesenha.Banco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Model.Conta;

import java.util.ArrayList;

public class ContaDAO {

    private static Context ctx;
    private static SQLiteDatabase db;

    public ContaDAO(Context ctx){
        this.ctx = ctx;
    }

    public static ArrayList<Conta> select(String[] id) throws Exception{
        db = new DatabaseHelper(ctx).getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT _ID, LOGIN, SENHA, CONTA, IDUSUARIO FROM " + DatabaseHelper.TABELA_CONTA + " WHERE IDUSUARIO = ?", id);
        cursor.moveToFirst();

        Log.e("conta", "TOTAL DE CONTAS " + cursor.getCount() + "");

        ArrayList<Conta> lista = new ArrayList<>();
        for (int x = 0; x < cursor.getCount(); x++) {
            lista.add(new Conta(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getInt(4)));
            Log.e("conta", cursor.getInt(0) + " - " +
                    cursor.getString(1) + " - " +
                    cursor.getString(2) + " - " +
                    cursor.getString(3) + " - " +
                    cursor.getInt(4));
            cursor.moveToNext();
        }

        cursor.close();

        return lista;
    }

    public static Conta selectConta(String[] parametros) throws Exception{
        db = new DatabaseHelper(ctx).getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT _ID, LOGIN, SENHA, CONTA FROM " + DatabaseHelper.TABELA_CONTA + " WHERE LOGIN = ? AND CONTA = ?", parametros);
        cursor.moveToFirst();

        Conta conta = new Conta(cursor.getInt(0),
                                cursor.getString(1),
                                cursor.getString(2),
                                cursor.getString(3));

        Log.e("conta", cursor.getInt(0) + " - " +
                cursor.getString(1) + " - " +
                cursor.getString(2) + " - " +
                cursor.getString(3));

        cursor.close();

        return conta;
    }

    public static void inserir(Conta conta) throws Exception{
        ContentValues values = new ContentValues();
        values.put("LOGIN", conta.getLogin());
        values.put("SENHA", conta.getSenha());
        values.put("CONTA", conta.getConta());
        values.put("IDUSUARIO", conta.getIdUsuario());

        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.insert(DatabaseHelper.TABELA_CONTA, null, values);
    }

    public static void update(Conta contaAntiga, Conta contaAlterada) throws Exception{
        ContentValues values = new ContentValues();

        if(!contaAntiga.getLogin().equals(contaAlterada.getLogin()))
            values.put("LOGIN", contaAlterada.getLogin());

        if(!contaAntiga.getSenha().equals(contaAlterada.getSenha()))
            values.put("SENHA", contaAlterada.getSenha());

        if(!contaAntiga.getConta().equals(contaAlterada.getConta()))
            values.put("CONTA", contaAlterada.getConta());

        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.update(DatabaseHelper.TABELA_CONTA, values, "_id = ?", new String[]{"" + contaAntiga.getId()});
    }

    public static void excluir (String id) throws Exception{
        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.delete(DatabaseHelper.TABELA_CONTA, "_ID = ?", new String[]{id});
    }

}