package com.android.br.projetogerenciadordesenha.Controller;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.ContaDAO;
import com.android.br.projetogerenciadordesenha.Model.Conta;

import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by mag.luiza on 03/12/2015.
 */
public class ControllerConta {

    public static void cadastrarConta(Conta conta, Context context){
        try{
            ContaDAO.inserir(conta);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM cadastrarConta() DE TELA.", Toast.LENGTH_LONG).show();
        }
    }
    public static void alterarConta(Conta contaAntiga, Conta contaAlterada, Context context){
        try{
            ContaDAO.update(contaAntiga, contaAlterada);
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM alterarConta() DE TELA.", Toast.LENGTH_LONG).show();
        }
    }
    public static String[] listarContas(final String ID_USUARIO, Context context){
        try{
            ArrayList<Conta> contas = ContaDAO.select(new String[]{ID_USUARIO});
            String[] todasContas = new String[contas.size()];

            for(int x=0; x<contas.size(); x++)
                todasContas[x] = "Login: " + contas.get(x).getLogin() +
                                 " Conta: " + contas.get(x).getConta();

            if(contas.size() == 0)
                return new String[] {"NÃO HÁ CONTAS CADASTRADAS"};

            return todasContas;
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM listarContas() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }
    public static Conta procurarConta(String login, String conta, Context context){
        try{
            Conta con = ContaDAO.selectConta(new String[]{login, conta});
            return con;
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM listarContas() DE TELA.", Toast.LENGTH_LONG).show();
            return null;
        }
    }
    public static void excluir(String login, String conta, Context context){
        try{
            ContaDAO.excluir(procurarConta(login, conta, context).getId()+"");
        } catch (Exception e){
            Toast.makeText(context, "ERRO EM excluir() DE TELA.", Toast.LENGTH_LONG).show();
        }
    }

}