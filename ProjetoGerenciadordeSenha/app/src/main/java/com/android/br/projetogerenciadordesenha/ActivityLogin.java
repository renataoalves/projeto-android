package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.UsuarioDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerUsuario;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener{

    private Button btnEntrar, btnCadastrarUsuarioLogin, btnEsqueciSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnEntrar = (Button) findViewById(R.id.btnEntrarLogin);
        btnEntrar.setOnClickListener(this);

        btnCadastrarUsuarioLogin = (Button) findViewById(R.id.btnCadastrarUsuarioLogin);
        btnCadastrarUsuarioLogin.setOnClickListener(this);

        btnEsqueciSenha = (Button) findViewById(R.id.btnEsqueciSenha);
        btnEsqueciSenha.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnEntrarLogin:
                String login = ((EditText) findViewById(R.id.txtLoginLogin)).getText().toString().trim();
                String senha = ((EditText) findViewById(R.id.txtSenhaLogin)).getText().toString().trim();

                try {
                    Log.e("contas", login + " -+- " + senha);

                    new UsuarioDAO(getApplicationContext());
                    String result[] = ControllerUsuario.validacaoLoginSenha(login, senha, this);
                    if (result[0].equals("0")) {
                        Toast.makeText(this, "Bem vindo " + result[2], Toast.LENGTH_LONG).show();
                        Intent it = new Intent(this, ActivityContas.class);
                        it.putExtra("ID_USUARIO", result[1]);
                        it.putExtra("LOGINUSUARIO", result[2]);
                        it.putExtra("EMAILUSUARIO", result[3]);
                        startActivity(it);
                    } else
                        Toast.makeText(this, "Usuário e/ou senha não correspondem", Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    Toast.makeText(this, "ERRO EM LOGIN DE USUÁRIO", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnCadastrarUsuarioLogin:
                    startActivity(new Intent(this, ActivityCadastroUsuario.class));
                break;
            case R.id.btnEsqueciSenha:
                    startActivity(new Intent(this, ActivityEsqueciSenhaEnviarCodigo.class));
                break;
            default:
                break;
        }
    }

}