package com.android.br.projetogerenciadordesenha.Banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mag.luiza on 02/12/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String NOME_BANCO = "SENHAS";
    private static final int VERSAO_BANCO = 1;
    protected static final String TABELA_USUARIO = " USUARIO ";
    protected static final String TABELA_CONTA = " CONTA ";

    public DatabaseHelper(Context context){
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABELA_USUARIO +
                " (_ID INTEGER PRIMARY KEY," +
                " LOGIN TEXT," +
                " SENHA TEXT," +
                " EMAIL TEXT," +
                " VALIDADOR TEXT);");

        db.execSQL("CREATE TABLE " + TABELA_CONTA +
                " (_ID INTEGER PRIMARY KEY," +
                " LOGIN TEXT," +
                " SENHA TEXT," +
                " CONTA TEXT," +
                " IDUSUARIO INTEGER, "+
                " FOREIGN KEY(IDUSUARIO) REFERENCES "+ TABELA_USUARIO +" (_ID));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABELA_USUARIO);
        db.execSQL("DROP TABLE IF EXIST " + TABELA_CONTA);
        onCreate(db);
    }

}