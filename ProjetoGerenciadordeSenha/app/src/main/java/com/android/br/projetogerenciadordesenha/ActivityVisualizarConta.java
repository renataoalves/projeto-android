package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.ContaDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerConta;
import com.android.br.projetogerenciadordesenha.Model.Conta;

public class ActivityVisualizarConta extends AppCompatActivity implements View.OnClickListener{

    private TextView lblLogin, lblSenha, lblConta;
    private Button btnAlterar, btnExcluir, btnVoltar;
    private String ID_USUARIO, LOGINCONTA, LOGINUSUARIO, CONTA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_conta);

        lblLogin = (TextView) findViewById(R.id.lblLoginVisualizar);
        lblSenha = (TextView) findViewById(R.id.lblSenhaVisualizar);
        lblConta = (TextView) findViewById(R.id.lblContaVisualizar);

        btnAlterar = (Button) findViewById(R.id.btnAlterarVisualizar);
        btnAlterar.setOnClickListener(this);

        btnExcluir = (Button) findViewById(R.id.btnExcluirVisualizar);
        btnExcluir.setOnClickListener(this);

        btnVoltar = (Button) findViewById(R.id.btnVoltarVisualizar);
        btnVoltar.setOnClickListener(this);

        buscarConta(getIntent());
    }

    private void buscarConta(Intent it){
        ID_USUARIO = it.getStringExtra("ID_USUARIO");
        LOGINUSUARIO = it.getStringExtra("LOGINUSUARIO");
        LOGINCONTA = it.getStringExtra("LOGINCONTA");
        CONTA = it.getStringExtra("CONTACONTA");

        try {
            new ContaDAO(getApplicationContext());
            Conta conta = ControllerConta.procurarConta(LOGINCONTA, CONTA, this);
            lblLogin.setText("LOGIN: " + conta.getLogin());
            lblSenha.setText("SENHA: " + conta.getSenha());
            lblConta.setText("CONTA: " + conta.getConta());
        } catch (Exception e){
            Toast.makeText(this, "ERRO EM BUSCA DE CONTA PARA VISUALIZAÇÃO.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnExcluirVisualizar:
                new ContaDAO(getApplicationContext());
                try {
                    ControllerConta.excluir(LOGINCONTA, CONTA, this);
                    Toast.makeText(this, "Conta excluída.", Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    Toast.makeText(this, "ERRO EM EXCLUSÃO DE CONTA.", Toast.LENGTH_LONG).show();
                }
                finish();
                break;
            case R.id.btnAlterarVisualizar:
                Intent it = new Intent(this, ActivityAlterarConta.class);
                it.putExtra("ID_USUARIO", ID_USUARIO);
                it.putExtra("LOGINUSUARIO", LOGINUSUARIO);
                it.putExtra("LOGINCONTA", LOGINCONTA);
                it.putExtra("CONTACONTA", CONTA);
                startActivity(it);
                break;
            case R.id.btnVoltarVisualizar:
                finish();
                break;
        }
    }

}