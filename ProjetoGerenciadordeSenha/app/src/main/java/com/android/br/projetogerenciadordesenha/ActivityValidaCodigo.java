package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityValidaCodigo extends AppCompatActivity implements View.OnClickListener{

    private Button btnConfirmarCodigo, btnCancelarCodigo;
    private String ID_USUARIO = "", CODIGO = "", LOGINUSUARIO = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valida_codigo);

        Intent it = getIntent();
        ID_USUARIO = it.getStringExtra("ID_USUARIO");
        String email = it.getStringExtra("EMAILUSUARIO");
        CODIGO = it.getStringExtra("CODIGOVALIDADOR");
        LOGINUSUARIO = it.getStringExtra("LOGINUSUARIO");

        ((TextView) findViewById(R.id.lblEmailEnviado)).setText("Código de verificação enviado para " + email);

        btnConfirmarCodigo = (Button) findViewById(R.id.btnConfirmarCodigo);
        btnConfirmarCodigo.setOnClickListener(this);

        btnCancelarCodigo = (Button) findViewById(R.id.btnCancelarCodigo);
        btnCancelarCodigo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnConfirmarCodigo:
                String codigoTela = ((EditText) findViewById(R.id.txtCodigoEmail)).getText().toString().trim();
                try {
                    if (CODIGO.equals(codigoTela)) {
                        Intent it = new Intent(this, ActivityEsqueciSenhaAlterarSenha.class);
                        it.putExtra("ID_USUARIO", ID_USUARIO);
                        it.putExtra("LOGINUSUARIO", LOGINUSUARIO);
                        startActivity(it);
                    } else
                        Toast.makeText(this, "Esse código de verificação não corresponde ao código enviado.", Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    Toast.makeText(this, "ERRO EM VALIDAR CÓDIGO.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnCancelarCodigo:
                startActivity(new Intent(this, ActivityLogin.class));
                break;
        }

    }

}