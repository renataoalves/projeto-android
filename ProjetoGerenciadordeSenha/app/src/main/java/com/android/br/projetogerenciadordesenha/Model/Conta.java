package com.android.br.projetogerenciadordesenha.Model;

/**
 * Created by mag.luiza on 02/12/2015.
 */
public class Conta {

    private int id;
    private String login;
    private String senha;
    private String conta;
    private int idUsuario;

    public Conta (int id, String login, String senha, String conta, int idUsuario){
        this.setId(id);
        this.setLogin(login);
        this.setSenha(senha);
        this.setConta(conta);
        this.setIdUsuario(idUsuario);
    }

    public Conta (int id, String login, String senha, String conta){
        this.setId(id);
        this.setLogin(login);
        this.setSenha(senha);
        this.setConta(conta);
    }

    public Conta (String login, String senha, String conta, int idUsuario){
        this.setLogin(login);
        this.setSenha(senha);
        this.setConta(conta);
        this.setIdUsuario(idUsuario);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConta() {
        return conta;
    }
    public void setConta(String conta) {
        this.conta = conta;
    }

    public int getIdUsuario() {
        return idUsuario;
    }
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}