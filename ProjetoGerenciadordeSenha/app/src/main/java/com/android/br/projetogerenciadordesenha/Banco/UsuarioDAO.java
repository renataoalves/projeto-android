package com.android.br.projetogerenciadordesenha.Banco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.br.projetogerenciadordesenha.Model.Usuario;

import java.util.ArrayList;
import java.util.Random;

public class UsuarioDAO {

    private static SQLiteDatabase db;
    private static ArrayList<Usuario> lista = new ArrayList<>();
    private static Context ctx;

    public UsuarioDAO(Context ctx) {
        this.ctx = ctx;
    }

    public static ArrayList<Usuario> select(String[] id) throws Exception{
        db = new DatabaseHelper(ctx).getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT _ID, LOGIN, SENHA, EMAIL, VALIDADOR FROM " + DatabaseHelper.TABELA_USUARIO + " WHERE LOGIN = ?", id);
        cursor.moveToFirst();

        Log.e("conta", cursor.getCount() + "");

        for(int x=0; x<cursor.getCount(); x++) {
            lista.add(new Usuario(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)));
            Log.e("conta", "ID+ "+cursor.getInt(0) + " - " +
                    "LOGIN+ "+cursor.getString(1) + " - " +
                    "SENHA+ "+cursor.getString(2) + " - " +
                    "EMAIL+ "+cursor.getString(3) + " - " +
                    "VALIDADOR+ "+cursor.getString(4));

            cursor.moveToNext();
        }

        cursor.close();

        return lista;
    }

    public static void insert(Usuario usuario) throws Exception{
        ContentValues values = new ContentValues();
        values.put("LOGIN", usuario.getLogin());
        values.put("SENHA", usuario.getSenha());
        values.put("EMAIL", usuario.getEmail());
        values.put("VALIDADOR", usuario.getValidador());

        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.insert(DatabaseHelper.TABELA_USUARIO, null, values);

        //select(new String[]{"RENATO"});
    }

    public void update(Usuario usuario, String login, String senha) throws Exception{
        ContentValues values = new ContentValues();

        if (!usuario.getLogin().equals(login))
            values.put("LOGIN", login);

        if (!usuario.getSenha().equals(senha))
            values.put("SENHA", senha);

        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.update(DatabaseHelper.TABELA_USUARIO, values, "_ID = ?", new String[]{"" + usuario.getId()});
    }

    public static String[] validacaoLoginSenha(String login, String senha) throws Exception{
        db = new DatabaseHelper(ctx).getReadableDatabase();

        String[] resposta = new String[4];

        Cursor cursor = db.rawQuery("SELECT _ID, LOGIN, EMAIL, SENHA FROM " + DatabaseHelper.TABELA_USUARIO + " WHERE LOGIN = ?", new String[]{login});
        cursor.moveToFirst();

        Log.e("contas", "LOGIN VÁLIDO? - QUANTIDADE " + cursor.getCount());

        if (cursor.getCount() == 1){
            Log.e("contas", "ID = " + cursor.getString(0));
            Log.e("contas", "LOGIN= "+ login + " = " + cursor.getString(1));
            Log.e("contas", "SENHA= "+ senha + " = " + cursor.getString(3));
            if(login.equals(cursor.getString(1)) && senha.equals(cursor.getString(3))) {
                resposta[0] = "0"; // status
                resposta[1] = cursor.getString(0); // id
                resposta[2] = cursor.getString(1); // login
                resposta[3] = cursor.getString(2); // email
            } else
                resposta[0] = "-1";
        } else
            resposta[0] = "-1";

        cursor.close();

        return resposta;
    }

    public static String validacaoLoginExiste(String login) throws Exception{
        db = new DatabaseHelper(ctx).getReadableDatabase();

        String resposta;

        Cursor cursor = db.rawQuery("SELECT LOGIN, EMAIL FROM " + DatabaseHelper.TABELA_USUARIO + " WHERE LOGIN = ?", new String[]{login});
        cursor.moveToFirst();

        Log.e("contas", "LOGIN EXISTE? - QUANTIDADE " + cursor.getCount());

        if (cursor.getCount() == 1){
            if(login.equals(cursor.getString(0))) {
                resposta = "-1"; // status
            } else
                resposta = "0";
        } else
            resposta = "0";

        cursor.close();

        return resposta;
    }

    public static String[] procurarEmail(String email) throws Exception{
        db = new DatabaseHelper(ctx).getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT _ID, LOGIN FROM " + DatabaseHelper.TABELA_USUARIO + " WHERE EMAIL = ?", new String[]{email});
        cursor.moveToFirst();

        String retorno = "-1";
        String login = "-1";

        if(cursor.getCount() != 0) {
            Log.e("contas", "EMAIL EXISTE? - QUANTIDADE " + cursor.getCount() + " - " + cursor.getString(0));
            retorno = cursor.getString(0);
            login = cursor.getString(1);
        } else
            Log.e("contas", "EMAIL NÃO EXISTE");

        cursor.close();
        return new String[]{retorno, login};
    }

    public static void alterarCodigo(String id, String codigo) throws Exception{
        ContentValues values = new ContentValues();

        values.put("VALIDADOR", codigo);

        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.update(DatabaseHelper.TABELA_USUARIO, values, "_id = ?", new String[]{id});
    }

    public static void alterarSenha(String id, String novaSenha) throws Exception{
        ContentValues values = new ContentValues();

        values.put("SENHA", novaSenha);

        db = new DatabaseHelper(ctx).getWritableDatabase();
        db.update(DatabaseHelper.TABELA_USUARIO, values, "_id = ?", new String[]{id});
    }

}