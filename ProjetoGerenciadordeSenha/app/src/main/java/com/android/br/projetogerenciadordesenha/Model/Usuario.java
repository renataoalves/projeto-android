package com.android.br.projetogerenciadordesenha.Model;

/**
 * Created by mag.luiza on 02/12/2015.
 */
public class Usuario {

    private long id;
    private String login;
    private String senha;
    private String email;
    private String validador;

    public Usuario(String login, String senha, String email){
        this.setLogin(login);
        this.setSenha(senha);
        this.setEmail(email);
    }

    public Usuario(String login, String senha, String email, String validador){
        this.setLogin(login);
        this.setSenha(senha);
        this.setEmail(email);
        this.setValidador(validador);
    }

    public Usuario(long id, String login, String senha, String email, String validador){
        this.setId(id);
        this.setLogin(login);
        this.setSenha(senha);
        this.setEmail(email);
        this.setValidador(validador);
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getValidador() {
        return validador;
    }
    public void setValidador(String validador) {
        this.validador = validador;
    }

}