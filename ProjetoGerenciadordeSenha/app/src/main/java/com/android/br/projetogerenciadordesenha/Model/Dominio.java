package com.android.br.projetogerenciadordesenha.Model;

/**
 * Created by mag.luiza on 11/12/2015.
 */
public enum Dominio {

    GMAIL,
    FACEBOOKMESSENGER,
    FACEBOOK,
    WHATSAPP,
    SNAP,
    SKYPY,
    SPOTIFY

}