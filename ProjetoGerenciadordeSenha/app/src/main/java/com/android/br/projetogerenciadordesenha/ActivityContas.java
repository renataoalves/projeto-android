package com.android.br.projetogerenciadordesenha;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.ContaDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerConta;

import java.util.List;

public class ActivityContas extends AppCompatActivity implements View.OnClickListener{

    private String ID_USUARIO = "";
    private String LOGINUSUARIO = "";
    private ArrayAdapter<String> adaptador;
    public static ListView listView;
    private Button btnNovaConta, btnSair;
    private AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contas);

        Intent it = getIntent();
        ID_USUARIO = it.getStringExtra("ID_USUARIO");
        LOGINUSUARIO = it.getStringExtra("LOGINUSUARIO");

        Log.e("contas", "ID_USUARIO ACTIVITY-CONTAS: " + ID_USUARIO);

        ((TextView) findViewById(R.id.lblLoginContasCadastradas)).setText(LOGINUSUARIO);

        btnNovaConta = (Button) findViewById(R.id.btnNovaConta);
        btnNovaConta.setOnClickListener(this);

        btnSair = (Button) findViewById(R.id.btnSair);
        btnSair.setOnClickListener(this);

        listView = (ListView) findViewById(R.id.listContasCadastradas);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogOpcoes(view);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new ContaDAO(getApplicationContext());
        adaptador = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, ControllerConta.listarContas(ID_USUARIO, this));
        adaptador.notifyDataSetChanged();
        listView.setAdapter(adaptador);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNovaConta:
                Intent it = new Intent(this, ActivityCadastroConta.class);
                it.putExtra("ID_USUARIO", ID_USUARIO);
                it.putExtra("LOGINUSUARIO", LOGINUSUARIO);
                startActivity(it);
                break;
            case R.id.btnSair:
                startActivity(new Intent(this, ActivityLogin.class));
                break;
        }
    }

    private void dialogOpcoes(View itemListView) {
        try {
            LayoutInflater li = getLayoutInflater();
            View view = li.inflate(R.layout.dialog_opcoes, null);

            TextView item = (TextView) itemListView;

            final Context context = this;
            final String[] parametros = tratarItemList(item.getText() + "");

            view.findViewById(R.id.btnVisualizarConta).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent it = new Intent(ActivityContas.this, ActivityVisualizarConta.class);
                    it.putExtra("ID_USUARIO", ID_USUARIO);
                    it.putExtra("LOGINUSUARIO", LOGINUSUARIO);
                    it.putExtra("LOGINCONTA", parametros[0]);
                    it.putExtra("CONTACONTA", parametros[1]);
                    startActivity(it);
                    alerta.dismiss();
                }
            });

            view.findViewById(R.id.btnAlterarConta).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent it = new Intent(ActivityContas.this, ActivityAlterarConta.class);
                    it.putExtra("ID_USUARIO", ID_USUARIO);
                    it.putExtra("LOGINUSUARIO", LOGINUSUARIO);
                    it.putExtra("LOGINCONTA", parametros[0]);
                    it.putExtra("CONTACONTA", parametros[1]);
                    startActivity(it);
                    alerta.dismiss();
                }
            });

            view.findViewById(R.id.btnExcluirConta).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    new ContaDAO (getApplicationContext());
                    ControllerConta.excluir(parametros[0], parametros[1], context);
                    Toast.makeText(ActivityContas.this, "Conta excluída.", Toast.LENGTH_SHORT).show();
                    startActivity(getIntent());
                    alerta.dismiss();
                }
            });

            view.findViewById(R.id.btnIrPara).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    irPara(parametros[1]);
                    alerta.dismiss();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("OPÇÕES");
            builder.setView(view);
            alerta = builder.create();
            alerta.show();
        } catch (ArrayIndexOutOfBoundsException e){
        }
    }

    private String[] tratarItemList(String textoItem){
        String[] item = textoItem.split(":");
        item[1] = item[1].substring(0, item[1].length()-5).trim();
        item[2] = item[2].trim();
        return new String[] {item[1], item[2]};
    }

    private void irPara(String app){

        Intent it = null;
        switch(app){
            case "GMAIL":
                it = getPackageManager().getLaunchIntentForPackage("com.google.android.gm");
                break;
            case "MESSENGER":
                it = getPackageManager().getLaunchIntentForPackage("com.facebook.orca");
                break;
            case "FACEBOOK":
                it = new Intent("android.intent.category.LAUNCHER");
                it.setClassName("com.facebook.katana", "com.facebook.katana.LoginActivity");
                break;
            case "WHATSAPP":
                it = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                break;
            case "SNAP":
                it = getPackageManager().getLaunchIntentForPackage("com.snapchat.android");
                break;
            case "SKYPE":
                it = getPackageManager().getLaunchIntentForPackage("com.skype.raider");
                break;
            case "SPOTIFY":
                it = new Intent(MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH);
                it.setComponent(new ComponentName("com.spotify.music", "com.spotify.music.MainActivity"));
                break;
        }

        try {
            startActivity(it);
        }catch (Exception e){
            Toast.makeText(this, "Esse aplicativo não está instalado em seu smartphone.", Toast.LENGTH_SHORT).show();
        }

    }

}