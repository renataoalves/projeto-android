package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.UsuarioDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerUsuario;

public class ActivityCadastroUsuario extends AppCompatActivity implements View.OnClickListener{

    private Button btCadastrar, btCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);

        btCadastrar = (Button) findViewById(R.id.btnCadastrarUsuario);
        btCadastrar.setOnClickListener(this);

        btCancelar = (Button) findViewById(R.id.btnCancelarCadastroUsuario);
        btCancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnCadastrarUsuario:
                cadastrarUsuario();
                break;
            case R.id.btnCancelarCadastroUsuario:
                finish();
                break;
        }
    }

    public void cadastrarUsuario(){
        String login = ((EditText) findViewById(R.id.txtLoginUsuario)).getText().toString().trim();
        String senha = ((EditText) findViewById(R.id.txtSenhaUsuario)).getText().toString().trim();
        String email = ((EditText) findViewById(R.id.txtEmailUsuario)).getText().toString().trim();

        Log.e("contas", login.trim().length() + "");

        try {
            if (!login.equals("") && login.trim().length() > 5) {
                if (!senha.equals("") && senha.length() > 5) {
                    if (!email.equals("") && email.contains("@")) {
                        new UsuarioDAO(getApplicationContext());
                        if (ControllerUsuario.validarLoginExiste(login, this).equals("0")) {
                            if (ControllerUsuario.validarEmailExiste(email, this)[0].equals("-1")) {
                                new UsuarioDAO(getApplicationContext());
                                ControllerUsuario.cadastrarUsuario(login, senha, email, this);

                                Toast.makeText(this, "Usuário cadastrado com sucesso!", Toast.LENGTH_LONG).show();

                                finish();
                            } else
                                Toast.makeText(this, "Email já está associado a um outro login!", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(this, "Login já existe!", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(this, "É necessário informar um EMAIL válido.", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(this, "É necessário informar uma SENHA válida com tamanho entre 6-15.", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(this, "É necessário informar um LOGIN válido com tamanho entre 6-10.", Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(this, "Usuário NÃO cadastrado! ERROR.", Toast.LENGTH_LONG).show();
        }
    }

}