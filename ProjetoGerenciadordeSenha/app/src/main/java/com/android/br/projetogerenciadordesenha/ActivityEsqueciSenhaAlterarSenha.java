package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Controller.ControllerUsuario;

public class ActivityEsqueciSenhaAlterarSenha extends AppCompatActivity implements View.OnClickListener{

    private String ID_USUARIO = "", LOGINUSUARIO = "";
    private Button btnAlterarSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueci_senha_alterar_senha);

        Intent it = getIntent();
        ID_USUARIO = it.getStringExtra("ID_USUARIO");
        LOGINUSUARIO = it.getStringExtra("LOGINUSUARIO");

        btnAlterarSenha = (Button) findViewById(R.id.btnAlterar);
        btnAlterarSenha.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String novaSenha = ((EditText) findViewById(R.id.txtSenhaNovaSenha)).getText().toString().trim();
        String novaSenhaConfima = ((EditText) findViewById(R.id.txtSenhaNovaSenhaConfirma)).getText().toString().trim();

        try {
            if (novaSenha.length() > 5 && novaSenhaConfima.length() > 5) {
                if (novaSenha.equals(novaSenhaConfima)) {
                    ControllerUsuario.alterarSenha(ID_USUARIO, novaSenha, this);
                    Toast.makeText(ActivityEsqueciSenhaAlterarSenha.this, "Senha alterada.", Toast.LENGTH_LONG).show();

                    Intent it = new Intent(this, ActivityContas.class);
                    it.putExtra("ID_USUARIO", ID_USUARIO);
                    it.putExtra("LOGINUSUARIO", LOGINUSUARIO);
                    startActivity(it);
                } else
                    Toast.makeText(this, "As senhas não correspondem.", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(this, "É necessário informar uma SENHA válida com tamanho entre 6-15.", Toast.LENGTH_LONG).show();
        } catch (Exception e){
            Toast.makeText(this, "ERRO EM ALTERAR SENHA!.", Toast.LENGTH_LONG).show();
        }
    }

}