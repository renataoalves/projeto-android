package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.ContaDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerConta;
import com.android.br.projetogerenciadordesenha.Model.Conta;

import java.util.ArrayList;

public class ActivityCadastroConta extends AppCompatActivity implements View.OnClickListener {

    private String ID_USUARIO = "", LOGINUSUARIO="";
    private Button btCadastrar, btCancelar;
    private Spinner spinnerContas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_conta);

        btCadastrar = (Button) findViewById(R.id.btnCadastrarConta);
        btCadastrar.setOnClickListener(this);

        btCancelar = (Button) findViewById(R.id.btnCancelarCadastroConta);
        btCancelar.setOnClickListener(this);

        spinnerContas = (Spinner) findViewById(R.id.spinnerContas);

        ArrayList<String> categories = new ArrayList<>();
        categories.add("Escolha seu domínio");
        categories.add("GMAIL");
        categories.add("MESSENGER");
        categories.add("FACEBOOK");
        categories.add("WHATSAPP");
        categories.add("SNAP");
        categories.add("SKYPE");
        categories.add("SPOTIFY");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerContas.setAdapter(adapter);

        Intent it = getIntent();
        ID_USUARIO = it.getStringExtra("ID_USUARIO");
        LOGINUSUARIO = it.getStringExtra("LOGINUSUARIO");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCadastrarConta:
                cadastrarConta();
                break;
            case R.id.btnCancelarCadastroConta:
                finish();
                break;
        }
    }

    public void cadastrarConta(){
        String login = ((EditText) findViewById(R.id.txtLoginConta)).getText().toString().trim();
        String senha = ((EditText) findViewById(R.id.txtSenhaConta)).getText().toString().trim();
        String conta = ((Spinner) findViewById(R.id.spinnerContas)).getSelectedItem().toString();

        try {
            if (!login.equals("")) {
                if(!senha.equals("")) {
                    if(!conta.equals("Escolha seu domínio")) {
                        new ContaDAO(getApplicationContext());
                        ControllerConta.cadastrarConta(new Conta(login, senha, conta, Integer.parseInt(ID_USUARIO)), this);
                        finish();

                        Toast.makeText(this, "Conta cadastrada com sucesso!", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(this, "Informe um DOMÍNIO!", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(this, "Informe uma SENHA!", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(this, "Informe um LOGIN!", Toast.LENGTH_LONG).show();
        } catch(Exception e){
            Toast.makeText(this, "Conta NÃO cadastrada! ERROR", Toast.LENGTH_LONG).show();
        }
    }

}