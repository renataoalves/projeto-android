package com.android.br.projetogerenciadordesenha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.br.projetogerenciadordesenha.Banco.ContaDAO;
import com.android.br.projetogerenciadordesenha.Controller.ControllerConta;
import com.android.br.projetogerenciadordesenha.Model.Conta;

import java.util.ArrayList;

public class ActivityAlterarConta extends AppCompatActivity implements View.OnClickListener{

    private String ID_USUARIO = "", LOGINCONTA = "", LOGINUSUARIO = "";
    private Button btnAlterar, btnCancelar;
    private Spinner spinnerContas;
    private int ID_CONTA = 0;
    private ArrayList<String> categories;

    private Conta contaAntiga, contaAlterada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_conta);

        spinnerContas = (Spinner) findViewById(R.id.spinnerAlterarConta);

        categories = new ArrayList<>();
        categories.add("GMAIL");
        categories.add("MESSENGER");
        categories.add("FACEBOOK");
        categories.add("WHATSAPP");
        categories.add("SNAP");
        categories.add("SKYPE");
        categories.add("SPOTIFY");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerContas.setAdapter(adapter);

        Intent it = getIntent();
        ID_USUARIO = it.getStringExtra("ID_USUARIO");
        LOGINCONTA = it.getStringExtra("LOGINCONTA");
        LOGINUSUARIO = it.getStringExtra("LOGINUSUARIO");

        btnAlterar = (Button) findViewById(R.id.btnAlterarConta);
        btnAlterar.setOnClickListener(this);

        btnCancelar = (Button) findViewById(R.id.btnCancelarAlterarConta);
        btnCancelar.setOnClickListener(this);

        preencherTela(it);
    }

    private void preencherTela(Intent it){
        try {
            String conta = it.getStringExtra("CONTACONTA");

            new ContaDAO(getApplicationContext());
            Conta contaBd = ControllerConta.procurarConta(LOGINCONTA, conta, this);

            String senha = contaBd.getSenha();

            ID_CONTA = contaBd.getId();

            ((EditText) findViewById(R.id.txtLoginAlterar)).setText(LOGINCONTA);
            ((EditText) findViewById(R.id.txtSenhaAlterar)).setText(senha);
            for (int x = 0; x < categories.size(); x++) {
                if (categories.get(x).equals(contaBd.getConta())) {
                    spinnerContas.setSelection(x);
                    break;
                }
            }

            contaAntiga = new Conta(ID_CONTA, LOGINCONTA, senha, conta);
        } catch (Exception e){
            Toast.makeText(this, "ERRO EM PREENCHIMENTO DE TELA.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnAlterarConta:
                String login = ((EditText) findViewById(R.id.txtLoginAlterar)).getText().toString().trim();
                String senha = ((EditText) findViewById(R.id.txtSenhaAlterar)).getText().toString().trim();
                String conta = ((Spinner) findViewById(R.id.spinnerAlterarConta)).getSelectedItem().toString();

                try {
                    if (!login.equals("")) {
                        if (!senha.equals("")) {
                            contaAlterada = new Conta(ID_CONTA, login, senha, conta);
                            new ContaDAO(getApplicationContext());
                            ControllerConta.alterarConta(contaAntiga, contaAlterada, this);
                            Toast.makeText(this, "Conta " + contaAlterada.getConta() + " alterada com sucesso!", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(this, "É necessário informar uma SENHA.", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(this, "É necessário informar um LOGIN.", Toast.LENGTH_LONG).show();
                } catch(Exception e){
                    Toast.makeText(this, "Conta NÃO alterada! ERROR.", Toast.LENGTH_LONG).show();
                }
            case R.id.btnCancelarAlterarConta:
                finish();
                break;
        }
    }

}